# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Spark-Jupyter Zoe application description generator."""

import json
import sys
import os

APP_NAME = 'spark-jupyter'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'master_mem_limit': {
        'value': (1024**3),
        'description': 'Spark Master memory limit (bytes)'
    },
    'worker_mem_limit':{
        'value': 12 * (1024**3),
        'description': 'Spark Worker memory limit (bytes)'
    },
    'notebook_mem_limit': {
        'value': 12 * (1024**3),
        'description': 'Notebook memory limit (bytes)'
    },
    'submit_mem_limit': {
        'value': 12 * (1024**3),
        'description': 'Spark submit memory limit (bytes)'
    },
    'worker_cores': {
        'value': 6,
        'description': 'Cores used by each worker'
    },
    'worker_count': {
        'value': 2,
        'description': 'Number of workers'
    }
}

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

MASTER_IMAGE = REPOSITORY + "/spark-master:" + VERSION
WORKER_IMAGE = REPOSITORY + "/spark-worker:" + VERSION
NOTEBOOK_IMAGE = REPOSITORY + "/spark-jupyter-notebook:" + VERSION

def spark_master_service(mem_limit):
    """
    :type mem_limit: int
    :type image: str
    :rtype: dict
    """
    service = {
        'name': "spark-master",
        'image': MASTER_IMAGE,
        'monitor': False,
        'resources': {
            "memory": {
                "min": mem_limit,
                "max": mem_limit
            },
            "cores": {
                'min': 1,
                'max': 1
            }
        },
        'ports': [
            {
                'name': "Spark master web interface",
                'protocol': "tcp",
                'port_number': 8080,
                'url_template': "http://{ip_port}/",
            }
        ],
        'environment': [
            ["SPARK_MASTER_IP", "{dns_name#self}"],
            ["HADOOP_USER_NAME", "{user_name}"],
            ["PYTHONHASHSEED", "42"],
            ["SPARK_HOME", "/opt/spark"]
        ],
        'volumes': [],
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1,
        'command': None
    }
    return service


def spark_worker_service(count, mem_limit, cores):
    """
    :type count: int
    :type mem_limit: int
    :type cores: int
    :rtype List(dict)

    :param count: number of workers
    :param mem_limit: hard memory limit for workers
    :param cores: number of cores this worker should use
    :return: a list of service entries
    """
    worker_ram = mem_limit - (1024 ** 3) - (512 * 1025 ** 2)
    service = {
        'name': "spark-worker",
        'image': WORKER_IMAGE,
        'monitor': False,
        'resources': {
            "memory": {
                "min": mem_limit,
                "max": mem_limit
            },
            "cores": {
                'min': cores,
                'max': cores
            }
        },
        'ports': [],
        'environment': [
            ["SPARK_WORKER_CORES", str(cores)],
            ["SPARK_WORKER_RAM", str(worker_ram)],
            ["SPARK_MASTER_IP", "{dns_name#spark-master0}"],
            ["SPARK_LOCAL_IP", "{dns_name#self}"],
            ["SPARK_HOME", "/opt/spark"],
            ["PYTHONHASHSEED", "42"],
            ["HADOOP_USER_NAME", "{user_name}"]
        ],
        'volumes': [],
        'total_count': count,
        'essential_count': 1,
        'startup_order': 1,
        'replicas': 1,
        'command': None
    }
    return service


def spark_jupyter_notebook_service(mem_limit, worker_mem_limit):
    """
    :type mem_limit: int
    :type worker_mem_limit: int
    :type hdfs_namenode: str
    :rtype: dict
    """
    driver_ram = (2 * 1024 ** 3)
    worker_ram = mem_limit - (1024 ** 3) - (512 * 1025 ** 2)
    service = {
        'name': "spark-jupyter",
        'image': NOTEBOOK_IMAGE,
        'monitor': True,
        'resources': {
            "memory": {
                "min": mem_limit,
                "max": mem_limit
            },
            "cores": {
                'min': 2,
                'max': 2
            }
        },
        'ports': [
            {
                'name': "Jupyter Notebook interface",
                'protocol': "tcp",
                'port_number': 8888,
                "url_template": "http://{ip_port}{proxy_path}",
                "proxy": True
            }
        ],
        'environment': [
            ["SPARK_MASTER", "spark://{dns_name#spark-master0}:7077"],
            ["SPARK_DRIVER_RAM", str(driver_ram)],
            ["SPARK_WORKER_RAM", str(worker_ram)],
            ["SPARK_HOME", "/opt/spark"],
            ["HADOOP_USER_NAME", "{user_name}"],
            ["PYTHONHASHSEED", "42"]
        ],
        'volumes': [],
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1,
        'command': None
    }
    return service


if __name__ == '__main__':
    ## Standard ZApp
    sp_master = spark_master_service(options['master_mem_limit']['value'])
    sp_worker = spark_worker_service(options['worker_count']['value'], options['worker_mem_limit']['value'], options['worker_cores']['value'])
    jupyter = spark_jupyter_notebook_service(options['notebook_mem_limit']['value'], options['worker_mem_limit']['value'])

    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            sp_master,
            sp_worker,
            jupyter
        ]
    }

    json.dump(app, open("spark.json", "w"), sort_keys=True, indent=4)

    ## CLOUDS ZAPP
    sp_master = spark_master_service(options['master_mem_limit']['value'])
    sp_worker = spark_worker_service(count=2, mem_limit=5 * (1024 ** 3), cores=2)
    jupyter = spark_jupyter_notebook_service(mem_limit=5 * (1024 ** 3), worker_mem_limit=5 * (1024 ** 3))

    sp_worker['labels'] = ['labs']
    sp_master['labels'] = ['labs']
    jupyter['labels'] = ['labs']

    app = {
        'name': 'clouds',
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            sp_master,
            sp_worker,
            jupyter
        ]
    }

    json.dump(app, open("clouds.json", "w"), sort_keys=True, indent=4)


    with open("images", "w") as fp:
        fp.write(MASTER_IMAGE + '\n')
        fp.write(WORKER_IMAGE + '\n')
        fp.write(NOTEBOOK_IMAGE + '\n')

    print("Two ZApps written")

