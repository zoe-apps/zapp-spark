#!/usr/bin/env bash

set -x

cat /opt/spark-defaults.conf | sed -e "s/XXX_DRIVER_MEMORY/$SPARK_DRIVER_RAM/" > ${SPARK_HOME}/conf/spark-defaults.conf

sudo -E -H -u $ZOE_USER mkdir -p $ZOE_WORKSPACE/.ipython/profile_default/startup/
sudo -E -H -u $ZOE_USER cp /opt/00-pyspark-setup.py $ZOE_WORKSPACE/.ipython/profile_default/startup/

