# Configure the necessary Spark environment
import os
import sys

# make sure pyspark tells workers to use python3 not 2 if both are installed
os.environ['PYSPARK_PYTHON'] = '/opt/conda/bin/python'
sys.path += ['/opt/spark/python', '/opt/spark/python/lib/py4j-0.10.7-src.zip']

if 'SPARK_MASTER' in os.environ:
	import pyspark
	conf = pyspark.SparkConf()

	# point to spark master
	conf.setMaster(os.environ["SPARK_MASTER"])

	# set other options as desired
	spark_executor_ram = int(os.environ["SPARK_WORKER_RAM"]) - (1024 ** 3) - (512 * 1024 ** 2)
	conf.set("spark.executor.memory", spark_executor_ram)

	# create the context
	sc = pyspark.SparkContext(conf=conf)
